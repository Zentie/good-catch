import { Component, Input, OnInit } from '@angular/core';
import { Service } from '../service';
import { Photos } from '../photos.model';
import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})

export class FilterComponent implements OnInit {

  keyWord: string;
  pageNumber: 1;
  photos: Photos[];

  constructor(private service: Service) { }

  ngOnInit() {}

  previousPage() {}

  nextPage() {}

  filterPhotos() {
    console.log('filtering photos...');
    this.service.getResult(this.keyWord, this.pageNumber)
    .subscribe(data => {
      this.photos = data;
    });
  }
}
